#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semWait(){
  int fd = running->syscall_args[0];

  #ifdef DEBUG
      printf("[INFO] pid=%d chiama semWait con fd=%d\n", running->pid, fd);
  #endif

  SemDescriptor* desc = SemDescriptorList_byFd(&running->sem_descriptors, fd);

  SYSCALL_ERROR_HANDLER(desc, DSOS_ESEMNUNSETROVA);

  SemDescriptorPtr* desc_ptr = desc->ptr;

  Semaphore* sem = desc->semaphore;

  PCB* p;
  sem->count--;

  if (sem->count < 0){
      List_detach(&sem->descriptors, (ListItem*) desc_ptr);
      List_insert(&sem->waiting_descriptors, sem->waiting_descriptors.last, (ListItem*) desc->ptr);
      running->status = Waiting;
      List_insert(&waiting_list, waiting_list.last, (ListItem*) running);
      p = (PCB*) List_detach(&ready_list, (ListItem*) ready_list.first);
      running = (PCB*) p;
  }

  running->syscall_retvalue = 0;
  return;
}
