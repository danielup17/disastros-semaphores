#include <stdio.h>
#include <unistd.h>
#include <poll.h>
//aggiunte
#include "disastrOS.h"
#include "disastrOS_constants.h"
#include "disastrOS_globals.h"
#include "disastrOS_semaphore.h"

#define PRODUCER 1
#define CONSUMER 0

#define PRODUCERS_SEM 1
#define CONSUMERS_SEM 2

#define WORK_SIZE 5
#define PROCESS_NUM 10

void producerJob(int producer_sem, int consumer_sem){
    for (int i = 0; i < WORK_SIZE; i++){
        disastrOS_semWait(producer_sem);
        printf(">> Produco una mela\n");
        Semaphore* semcons =  SemaphoreList_byId(&semaphores_list, 2);
        disastrOS_semPost(consumer_sem);
        printf("Ho %d Mele\n" , semcons->count);
        disastrOS_sleep(5);
    }
}

void consumerJob(int producer_sem,int consumer_sem){
    for (int i = 0; i < WORK_SIZE; i++){
        disastrOS_semWait(consumer_sem);
        printf("<< Mangio una mela\n");
        Semaphore* semcons =  SemaphoreList_byId(&semaphores_list, 2);
        disastrOS_semPost(producer_sem);
        printf("Ho %d Mele\n" , semcons->count);
        disastrOS_sleep(5);
    }
}
// we need this to handle the sleep state
void sleeperFunction(void* args){
  printf("Hello, I am the sleeper, and I sleep %d\n",disastrOS_getpid());
  while(1) {
    getc(stdin);
    disastrOS_printStatus();
  }
}

void childFunction(void* args){
  printf("[INFO] Hello, I am the child function %d\n", disastrOS_getpid());
  int type=0;
  int mode=0;
  int fd=disastrOS_openResource(disastrOS_getpid(), type, mode);
  printf("[INFO] Apro risorse con fd=%d\n", fd);

  printf("[INFO] Apro semafori\n");

  int prod_id = disastrOS_semOpen(PRODUCERS_SEM, PROCESS_NUM);  
  int cons_id = disastrOS_semOpen(CONSUMERS_SEM,0);

  printf("[INFO] Aspetto un po' :)\n");
  disastrOS_sleep(25);

  if (disastrOS_getpid() % 2 == 0)
    producerJob(prod_id, cons_id);
  else
    consumerJob(prod_id, cons_id);
  printf("[INFO] pid=%d è terminato\n", disastrOS_getpid());

  printf("[INFO] Chiudo i semafori\n");
  disastrOS_semClose(prod_id);
  disastrOS_semClose(cons_id);
  disastrOS_exit(disastrOS_getpid()+1);
}


void initFunction(void* args) {
  disastrOS_printStatus();
  printf("[INFO] hello, I am init and I just started\n");
  disastrOS_spawn(sleeperFunction, 0);
  printf("[INFO] I feel like to spawn 4 nice threads\n");
  int alive_children=0;

  for (int i=0; i<=3; ++i) {
    int type=0;
    int mode=DSOS_CREATE;
    // printf("mode: %d\n", mode);
    // printf("opening resource (and creating if necessary)\n");
    int fd=disastrOS_openResource(i,type,mode);
    printf("[INFO] fd=%d\n", fd);
    disastrOS_spawn(childFunction, 0);
    alive_children++;
  }

  disastrOS_printStatus();
  int retval;
  int pid;
  while(alive_children>0 && (pid=disastrOS_wait(0, &retval))>=0){
   // disastrOS_printStatus();
    //printf("initFunction, child: %d terminated, retval:%d, alive: %d \n",
	  // pid, retval, alive_children);
    --alive_children;
  }
  disastrOS_printStatus();
  printf("***SHUTDOWN!***\n");
  disastrOS_shutdown();
}

int main(int argc, char** argv){
  char* logfilename=0;
  if (argc>1) {
    logfilename=argv[1];
  }
  // we create the init process processes
  // the first is in the running variable
  // the others are in the ready queue
  printf("the function pointer is: %p", childFunction);
  // spawn an init process
  printf("start\n");
  disastrOS_start(initFunction, 0, logfilename);
  return 0;
}
